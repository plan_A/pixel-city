//
//  Constants.swift
//  pixel-city
//
//  Created by Jenia on 6/28/18.
//  Copyright © 2018 Jenia. All rights reserved.
//

import Foundation

let apiKey = "3529cd340da699132eebd384be28aa0b"

func flickrUrl(forApiKey key : String, withAnnotation annotation: DroppablePin, andNumberOfPhotoes number: Int) -> String{
    let url = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&lat=\(annotation.coordinate.latitude)&lon=\(annotation.coordinate.longitude)&radius=1&radius_units=mi&per_page=\(number)&format=json&nojsoncallback=1"
    print(url)
    return url
}


