//
//  PhotoCell.swift
//  pixel-city
//
//  Created by Jenia on 6/28/18.
//  Copyright © 2018 Jenia. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame:frame)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
